<?php
$resVar = $_POST['search-inp'] ;
    $json = file_get_contents('back/images-data.json');
    $res = array();// куда сохраняем результат
    if(isset($json) && isset($resVar)) {
      $obj = json_decode($json);
      foreach ($obj as $key =>$item) {
          foreach ($item as $itemKey=> $itemVal) {
            if(is_array($itemVal)) {
              foreach ($itemVal as $itemArr => $itemArrVal) {
                if($itemArrVal === $resVar) {
                  array_push($res,$item);
                }
              }
            } else {
                if($itemVal === $resVar) {
                  array_push($res,$item);
                }
              }
            }
            }
    } else {
      array_push($res,'Ничего не найдено');
    }


echo(
 '<!DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Интернет-фотоальбом</title>

     <link rel="shortcut icon" href="images/main/favicon.png" type="image/png">
     <link rel="stylesheet" href="css/style.css">
   </head>
   <body>
     <!-- header section start -->
   <header class="header">
     <!-- navigation menu start -->
     <nav>
       <ul class="menu-list">
         <li>
           <a href="pages/categories.html">Категории</a>
           <ul class="menu-sub-list">
             <li>
               <a href="pages/categories/business.html">Бизнесс</a>
             </li>
             <li>
               <a href="pages/categories/nature.html">Природа</a>
             </li>
             <li>
               <a href="pages/categories/sport.html">Спорт</a>
             </li>
           </ul>
         </li>
         <li>
           <a href="pages/users.html">Пользователи</a>
         </li>
         <li>
           <a href="pages/contacts.html">Контакты</a>
         </li>
       </ul>
     </nav>
     <!-- navigation menu end -->
     <!-- logotype start -->
     <div class="logo-wrap">
         <div class="logo"></div>
     </div>
     <!--  logotype end -->
     <!-- autorization section start -->
     <div class="autorization-wrap">
       <a href="">Войти</a>
       <a href="">Регистрация</a>
     </div>
     <!-- autorization section end -->
   </header>
   <!-- header slider section start -->
   <main class="res">
   <h1>Результат поиска:</h1>');
    if(count($res) > 0) {
      foreach($res as $key => $val) {
        echo (
          '<div>
            <img src="'.$val->src.'"/>
            <p>Автор: '.$val->author.'
            </p>
          </div>

          ');
      }
    } else {
      echo ('<h2>Ничего не найдено</h2>');
    }
   echo('
   </main>
   <!-- footer section start -->
   <footer>
     <h2>
       Свяжитесь с нами. <a href="" class="link-modal-contact">Тыц</a>
     </h2>
     <form class="search-wrap" action="result-search.php" method="post">
       <input type="search" placeholder="Поиск картинки" required="required" name="search-inp">
       <button type="submit" name="button" class="sbm"><i></i></button>
     </form>
     <div class="soc-wrap">
       <a href="">
         <i class="soc f"></i>
       </a>
       <a href="">
         <i class="soc i"></i>
       </a>
     </div>
     <p>
       &copy; 2017 Интернет-фотоальбом. <small>Разработал: <a href="https://www.facebook.com/profile.php?id=100005633058396&ref=bookmarks">М. Пушкарёв</a></small>
     </p>
   </footer>
   <!-- footer section end -->
   <!-- section modals windows start -->
   <div class="modal-outer">
     <div class="modal-contact">
       <form name="form-contact" method="post">
         <input type="text" name="firstname" class="firstname" placeholder="Имя" required="required">
         <input type="tel" name="phone" class="phone" placeholder="Телефон" required="required">
         <button type="submit" name="button" class="btn">Связаться</button>
       </form>
     </div>
   </div>
   <!-- section modals widows end -->
   <!-- include scripts start -->
   <script src="scripts/jquery-3.2.1.min.js"></script>
   <script src="scripts/slick.js"></script>
   <script src="scripts/script.js"></script>
   </body>
 </html>');
  ?>
