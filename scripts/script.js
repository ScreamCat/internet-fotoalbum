$(function() {
  // header slider
  $('.header-slider').slick({
    appendArrows: false,
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 1000,
    pauseOnHover: false,
  });

  // examples images slider
  $('.examples-images-slider').slick({
    appendArrows: false,
    infinite: true,
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 1000,
    pauseOnHover: false,
  });

  // filter category images
  $('.fltr-input').keyup(function() {
    let value = this.value.toLowerCase(); // value from input
    let images = $('.category-all-img-wrap img');
    if(value !== '') {
      console.log(value);
      $.each(images, function(i, elem) {
        let filtrVal = $(elem).data('filtr').toLowerCase();
        if (filtrVal.indexOf(value) !== -1) {
          $(elem).removeClass('filtered');
        } else {
          $(elem).addClass('filtered');
        }
      });
    } else {
      images.removeClass('filtered');
    }
  });

  // show more inform about user
  $('.user-miniatures').click(function(e) {
    let works = $(this).siblings('.user-works');
    $(works).fadeIn().css('height', '200px')
    e.preventDefault();
    return false;
  })

  //open modal window
  $('.link-modal-contact').click(function(e) {
    $('.modal-outer').addClass('active');
    e.preventDefault();
    return false;
  });

  //hide modal window
  $('.modal-outer').click(function() {
    $(this).removeClass('active');
  }).children().click(function(e) { //stop event click from parent
    e.stopPropagation();
  });

  //send data from form-contact
  $('form[name="form-contact"]').submit(function(e){
    let inputs = $(e.target).children('input');
    let dataRes = []; //arr for result
    let stringRes = 'Отправлено:\n'
    $.each(inputs, function(i, elem) {
      dataRes.push(
        {
          [$(elem).attr('name')] : $(elem).val()
        }
      );
      stringRes += [$(elem).attr('name')] +' : '+ $(elem).val() + '\n';
    });
    alert(stringRes);
    $('.modal-outer').removeClass('active'); //close modal window
    e.preventDefault();
    return false;
  });


//mask for search input
  $('.search-wrap input').keypress(function(e) {
    let reg = /[а-я]/;
    if(reg.exec(e.key) === null) {
      return false;
    }
  });

  //mask for phone input
  $('input[type="tel"]').keypress(function(e) {
    let reg = /[1-9]/;
    if(reg.exec(e.key) === null) {
      return false;
    }
  });
});
